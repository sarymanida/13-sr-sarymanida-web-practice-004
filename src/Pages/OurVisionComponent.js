import React from "react";
import { Card, Col,Row } from "react-bootstrap";

export default function OurVisionComponent() {
  return (
    <div style={{ paddingTop: "35px" }}>
      <Row>
        <Col lg={6} md={12} xs={12}>
          <Card className="mb-4 bg-light text-dark round border-0 rounded-0" style={{height:"13rem" , lineHeight:"30px"}}>
            <Card.Body>
              <Card.Text as="h3" style={{ textAlign: "center" }}>
                Vision
              </Card.Text>
              <ul>
                <li>
                  To be the best SW Professional Training Center in Cambodia
                  Mission
                </li>
              </ul>
            </Card.Body>
          </Card>
        </Col>
        <Col lg={6} md={12} xs={12}>
          <Card className="mb-4 bg-light text-dark round border-0 rounded-0" style={{height:"13rem" , lineHeight:"30px"}}>
            <Card.Body>
              <Card.Text as="h3" style={{ textAlign: "center" }}>
                Mission
              </Card.Text>
              <ul>
                <li>High quality training and research</li>
                <li>
                  Developing Capacity of SW Experts to be Leaders in IT Field
                </li>
                <li>Developing sustainable ICT Program</li>
              </ul>
            </Card.Body>
          </Card>
        </Col>
      </Row>
      <Row>
        <Col lg={6} md={12} xs={12}>
          <Card className="mb-4 bg-light text-dark round border-0 rounded-0" style={{height:"13rem" , lineHeight:"30px"}}>
            <Card.Body>
              <Card.Text as="h3" style={{ textAlign: "center" }}>
                Strategy
              </Card.Text>
              <ul>
                <li>
                  Best training method with up to date curriculum and
                  environment
                </li>
                <li>
                  Cooperation with the best IT industry to guarantee student's
                  career and benefits
                </li>
                <li>Additional Soft Skill, Management, Leadership training</li>
              </ul>
            </Card.Body>
          </Card>
        </Col>
        <Col lg={6} md={12} xs={12}>
          <Card className="mb-4 bg-light text-dark round border-0 rounded-0" style={{height:"13rem" , lineHeight:"30px"}}>
            <Card.Body>
              <Card.Text as="h3" style={{ textAlign: "center" }}>
                Slogan
              </Card.Text>
              <ul>
                <li>
                  "KSHRD, connects you to various opportunities in IT Field."
                </li>
                <li>
                  Raising brand awareness with continuous advertisement of SNS
                  and any other media
                </li>
              </ul>
            </Card.Body>
          </Card>
        </Col>
      </Row>
    </div>
  );
}
