import React from "react";
import { Card, Col, Container, Navbar, Row } from "react-bootstrap";
import img from "../img/logo.png";

export default function FooterComponent() {
  return (
    <div className="FooterNav">
      <Navbar collapseOnSelect expand="lg" bg="light" variant="light">
        <Container>
          <Row>
            <Col lg={4} md={4} xs={4}>
              <div className="Footer">
                <Card.Img
                  className="ImageHRD"
                  variant="top"
                  src={img}
                  style={{ width: "150px", height: "150px" }}
                />
                <p>
                  <b>&copy; រក្សាសិទ្ធិគ្រប់យ៉ាងដោយ KSHRD Center ឆ្នាំ២០២២</b>
                </p>
              </div>
            </Col>
            <Col lg={4} md={4} xs={4}>
              <div className="Footer">
                <h3>Address</h3>
                <p>
                  <span>
                    <b>Address:</b>
                  </span>{" "}
                  #12, St 323, Sangkat Boeung Kak II, Khan Toul Kork, Phnom
                  Penh, Cambodia.
                </p>
              </div>
            </Col>
            <Col lg={4} md={4} xs={4}>
              <div className="Footer">
                <h3>Contact</h3>
                <p>
                  <span>
                    <b>Tel: </b>
                  </span>
                  012 998 919 (Khmer)
                </p>
                <p>
                  <span>
                    <b>Tel: </b>
                  </span>
                  085 402 605 (Korean)
                </p>
                <p>
                  <span>
                    <b>Email: </b>
                  </span>
                  info.kshrd@gmail.com phirum.gm@gmail.com
                </p>
              </div>
            </Col>
          </Row>
        </Container>
      </Navbar>
    </div>
  );
}
