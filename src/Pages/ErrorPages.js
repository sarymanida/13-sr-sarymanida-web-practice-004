import React from "react";
import { Container } from "react-bootstrap";

function ErrorPages() {
  return (
    <Container>
      <div
        style={{
          textAlign: "center",
          height: "28rem",
          paddingTop: "200px",
          color: "red",
        }}
      >
        <h1>No Pages</h1>
        <h1>Exit !</h1>
      </div>
    </Container>
  );
}

export default ErrorPages;
