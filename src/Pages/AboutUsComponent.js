import React from "react";
import { Card, Col, Row } from "react-bootstrap";

function AboutUsComponent() {
  return (
    <div className="row" style={{ paddingTop: "35px" }}>
      <Row>
        <Col lg={6} md={12} xs={12}>
          <div className="w-100 h-100">
            <Card.Img
              className="border-0 rounded-0 w-100 h-100"
              variant="top"
              src="https://kshrd.com.kh/static/media/1.6169e38f.jpg"
            />
          </div>
        </Col>
        <Col lg={6} md={12} xs={12}>
          <div
            className="p-3 mb-2 bg-light text-dark border-0 rounded-0 h-100"
            style={{ lineHeight: "2em" }}
          >
            <p>
              Korea Software HRD Center is an academy training center for
              training software professionals in cooperation with Korea
              International Cooperation Agency(KOICA) and Webcash in April, 2013
              in Phnom Penh, Cambodia. From 2020, Korea Software HRD Center has
              been become Global NGO with the name Foundation for Korea Software
              Global Aid (KSGA), main sponsored by Webcash Group, to continue
              mission for ICT Development in Cambodia and will recruit 60 t0 80
              scholarship students every year.
            </p>
          </div>
        </Col>
      </Row>
      <Row style={{paddingTop:"20px"}}>
        <Col lg={6} md={12} xs={12}>
          <div
            className="p-3 mb-2 bg-light text-dark border-0 rounded-0 h-100 p-3"
            style={{ lineHeight: "2em" }}
          >
            <p>
              Korea Software HRD Center is an academy training center for
              training software professionals in cooperation with Korea
              International Cooperation Agency(KOICA) and Webcash in April, 2013
              in Phnom Penh, Cambodia. From 2020, Korea Software HRD Center has
              been become Global NGO with the name Foundation for Korea Software
              Global Aid (KSGA), main sponsored by Webcash Group, to continue
              mission for ICT Development in Cambodia and will recruit 60 t0 80
              scholarship students every year.
            </p>
          </div>
        </Col>
        <Col lg={6} md={12} xs={12}>
          <div className="w-100 h-100">
            <Card.Img
              className="border-0 rounded-0 w-100 h-100"
              variant="top"
              src="https://kshrd.com.kh/static/media/1.6169e38f.jpg"
            />
          </div>
        </Col>
      </Row>
      <Row style={{paddingTop:"20px"}}>
        <Col lg={6} md={12} xs={12}>
          <div className="w-100 h-100">
            <Card.Img
              className="border-0 rounded-0 w-100 h-100"
              variant="top"
              src="https://kshrd.com.kh/static/media/1.6169e38f.jpg"
            />
          </div>
        </Col>
        <Col lg={6} md={12} xs={12}>
          <div
            className="p-3 mb-2 bg-light text-dark border-0 rounded-0 h-100 p-3"
            style={{ lineHeight: "2em" }}
          >
            <p>
              Korea Software HRD Center is an academy training center for
              training software professionals in cooperation with Korea
              International Cooperation Agency(KOICA) and Webcash in April, 2013
              in Phnom Penh, Cambodia. From 2020, Korea Software HRD Center has
              been become Global NGO with the name Foundation for Korea Software
              Global Aid (KSGA), main sponsored by Webcash Group, to continue
              mission for ICT Development in Cambodia and will recruit 60 t0 80
              scholarship students every year.
            </p>
          </div>
        </Col>
      </Row>
    </div>
  );
}

export default AboutUsComponent;
