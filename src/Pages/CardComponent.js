import React from "react";
import { Card, Button } from "react-bootstrap";

function CardComponent({ a }) {
  return (
    <div className="cardTop">
      <h1>Trending Courses</h1>
      <div className="cardTable">
        {a?.map((prev) => (
          <Card style={{ width: "19rem", margin: "10px", textAlign: "center" }}>
            <Card.Img variant="top" src={prev.thumbnail} />
            <Card.Body>
              <Card.Title>{prev.title}</Card.Title>
              <Card.Text>{prev.description}</Card.Text>
              <Card.Text>
                <Card.Text>{prev.price}</Card.Text>
              </Card.Text>
              <Button variant="primary">Go somewhere</Button>
            </Card.Body>
          </Card>
        ))}
      </div>
    </div>
  );
}

export default CardComponent;
